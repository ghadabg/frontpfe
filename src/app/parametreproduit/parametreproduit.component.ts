import { Component, OnInit } from '@angular/core';
import { ParametreProduit } from './ParametreProduit.model';

@Component({
  selector: 'app-parametreproduit',
  templateUrl: './parametreproduit.component.html',
  styleUrls: ['./parametreproduit.component.css']
})
export class ParametreproduitComponent implements OnInit {
ProduitArray=[]
parametreProduit=new ParametreProduit();
  constructor() { }

  ngOnInit(): void {
  }
  addparametreProduit(){
    this.parametreProduit=new ParametreProduit();  
    this.ProduitArray.push(this.parametreProduit);
  }    
}
