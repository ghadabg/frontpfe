import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MailComponent } from './mail/mail.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AdminComponent } from './admin/admin.component';
import { ParametresComponent } from './parametres/parametres.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TableComponent } from './table/table.component';
import { RepeatfieldComponent } from './repeatfield/repeatfield.component';
import { CanalComponent } from './canal/canal.component';
import { ParametrecompteComponent } from './parametrecompte/parametrecompte.component';
import { ParametreproduitComponent } from './parametreproduit/parametreproduit.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    MailComponent,
    SideBarComponent,
    AdminComponent,
    ParametresComponent,
    NotfoundComponent,
    HeaderComponent,
    FooterComponent,
    TableComponent,
    RepeatfieldComponent,
    CanalComponent,
    ParametrecompteComponent,
    ParametreproduitComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
