import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametreproduitComponent } from './parametreproduit.component';

describe('ParametreproduitComponent', () => {
  let component: ParametreproduitComponent;
  let fixture: ComponentFixture<ParametreproduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametreproduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametreproduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
