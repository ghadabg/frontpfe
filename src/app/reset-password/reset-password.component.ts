import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  @Output() mail:string;
  constructor(private router:Router) {}
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  navigate(){
    this.router.navigate(['login']);
    console.log("SUCCESS");
  }
  navigatemail(){
    this.router.navigate(['mail']);
    console.log("SUCCESS");
  }
}
