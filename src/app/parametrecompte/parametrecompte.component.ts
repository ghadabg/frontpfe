import { Component, OnInit } from '@angular/core';
import { parametreCompte } from './parametreCompte.model';
import { preference } from '../preference.model';

@Component({
  selector: 'app-parametrecompte',
  templateUrl: './parametrecompte.component.html',
  styleUrls: ['./parametrecompte.component.css']
})
export class ParametrecompteComponent implements OnInit {
  compteArray=[];
  parametrecompte=new parametreCompte();
  preference=new preference()
  DattaArray=[]
  constructor() { }

  ngOnInit(): void {
  }
  addparametrecompte(){
    this.parametrecompte=new parametreCompte();  
    this.compteArray.push(this.parametrecompte);
  }    
    }
