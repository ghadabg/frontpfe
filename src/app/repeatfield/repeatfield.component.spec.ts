import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepeatfieldComponent } from './repeatfield.component';

describe('RepeatfieldComponent', () => {
  let component: RepeatfieldComponent;
  let fixture: ComponentFixture<RepeatfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepeatfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepeatfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
