import { Component, OnInit } from '@angular/core';
import { canal } from '../canal.model';

@Component({
  selector: 'app-canal',
  templateUrl: './canal.component.html',
  styleUrls: ['./canal.component.css']
})
export class CanalComponent implements OnInit {
  canal=new canal();
  mailarray=[];
  change=false;
  constructor() { }

  ngOnInit(): void {}
  addcanal(){
    this.canal=new canal();
    this.mailarray.push(this.canal);
  }
  back(index){this.mailarray.splice(index);
}
}
